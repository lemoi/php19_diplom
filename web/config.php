<?php
    $config = [];
    if (isHeroku()) {
        $dbopts = parse_url(getenv('CLEARDB_DATABASE_URL'));
        $mysql = [
            'host' => $dbopts["host"],
            'user' => $dbopts["user"],
            'pass' => $dbopts["pass"],
            'port' => $dbopts["port"],
            'dbname' => ltrim($dbopts["path"],'/'),
        ];
    } else {
        $mysql = [
            'host' => 'localhost',
            'user' => 'root',
            'pass' => 'root',
            'dbname' => 'faq',
        ];
    }
    $config['mysql'] = $mysql;

    return $config;
