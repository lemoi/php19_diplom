<?php

class Model
{

    protected $db = null;
    protected $table = 'model';
    protected $fields = [];
    protected $autoFields = [];

    function __construct()
    {
        $this->db = DataBase::getCoonection();
    }

    function getFields()
    {
        return array_keys($this->fields);
    }

    /**
     * Добавление объекта
     * @param $params array
     * @return mixed
     */
    function add($params)
    {
        $keys = array_keys($this->fields);
        foreach ($this->autoFields as $key) {
            if (empty($params[$key])) {
                $keys = array_diff($keys, [$key]); // удалим данное поле из обработки
            }
        }

        $query = 'INSERT INTO `' . $this->table . '`(' . implode(', ', $keys) . ')'
            .' VALUES(:' . implode(', :', $keys) . ')';

        $sth = $this->db->prepare($query);

        foreach ($keys as $key) {
            $sth->bindValue(':' . $key, $params[$key], $this->fields[$key]);
        }

        //return $sth->execute();
        $result = $sth->execute();
        if ($result !== false) {
            $result = $this->db->lastInsertId();
            //addLog('add ' . $this->table . ' (' . $result . ')');
        }
        return $result;
    }

    /**
     * Удаление объекта
     * @param $id int
     * @return mixed
     */
    function delete($id)
    {
        $sth = $this->db->prepare("DELETE FROM `{$this->table}` WHERE id=:id");
        $sth->bindValue(':id', $id, PDO::PARAM_INT);
        $result = $sth->execute();
        if ($result !== false) {
            //addLog('delete ' . $this->table . ' (' . $id . ')');
        }
        return $result;
    }

    /**
     * Обновление объекта
     * @param $id int
     * @param $params array
     * @return mixed
     */
    function update($id, $params)
    {
        $params = array_intersect_key($params, $this->fields);

        $update = [];
        foreach ($params as $key => $value) {
            $update[] = $key . '`=:' . $key;
        }

        if (count($update) == 0) {
            return false;
        }

        $query = 'UPDATE `'.$this->table . '` SET `' . implode(', `', $update) . ' WHERE `id`=:id';
        $sth = $this->db->prepare($query);

        foreach ($params as $key => $value) {
            if (isset($value)) {
                $sth->bindValue(':' . $key, $value, $this->fields[$key]);
            }
        }
        $sth->bindValue(':id', $id, PDO::PARAM_INT);

        $result = $sth->execute();
        if ($result !== false) {
            //addLog('update ' . $this->table . ' (' . $id . ')');
        }
        return $result;
    }

    /**
     * Получение одного объекта
     * @param $id int
     * @return array
     */
    public function find($id)
    {
        $keys = array_keys($this->fields);
        $sql = 'SELECT `id`, `' . implode('`, `', $keys) . '` FROM `' . $this->table . '`  WHERE id=:id';
        $sth = $this->db->prepare($sql);
        $sth->bindValue(':id', $id, PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    /**
     * Получение всех объектов
     * @return array
     */
    public function findAll()
    {
        $keys = $this->getFields();
        $query = 'SELECT `id`, `' . implode('`, `', $keys) . '` FROM `' . $this->table . '`';
        $sth = $this->db->prepare($query);
        if ($sth->execute()) {
            return $sth->fetchAll();
        }
        return false;
    }

    /*
     * Получение представления объекта
     * @param $id int
     * @return string
     */
    public function getView($id)
    {
        $result = $this->find($id);
        if ($result === false) {
            return "{$this->table} имеющий имеющий id = {$id} в БД отсутствует";
        }

        $view = [
            array_key_exists('name', $result) ? "\"{$result['name']}\"" : "",
            "({$id})"
        ];
        return implode(' ', $view);
    }
}