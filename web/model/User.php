<?php

require_once 'Model.php';

class User extends Model
{
	protected $table = 'user';
	protected $fields = [
	    'login' => PDO::PARAM_STR,
        'password' => PDO::PARAM_STR
    ];

	/**
	 * Получение одного пользователя
	 * @param $id int
	 * @return array
	 */
	public function find($searchField)
	{
        $keys = array_keys($this->fields);
        if (!is_array($searchField)) {
            $searchField = ['id' => $searchField];
        }
        $fields = [];
        foreach ($searchField as $key => $value) {
            $fields[] = $key . '`=:' . $key;
        }
        //$query = 'SELECT `id`, `' . implode('`, `', $keys) . '` FROM `' . $this->table . '` WHERE id=:id';
        $query = 'SELECT `id`, `' . implode('`, `', $keys) . '` FROM `' . $this->table . '`  WHERE `' . implode(' AND `', $fields);
        $sth = $this->db->prepare($query);
        //$sth->bindValue(':id', $id, PDO::PARAM_INT);
        foreach ($searchField as $key => $value) {
            $sth->bindValue(':' . $key, $value, $key === 'id' ? PDO::PARAM_INT : $this->fields[$key]);
        }
		$sth->execute();
		$result = $sth->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getView($id)
    {
        $result = $this->find($id);
        if ($result === false) {
            return "администратор имеющий id = ${id} в БД отсутствует";
        }

        return "\"${result['login']}\" (${id})";
    }
}
