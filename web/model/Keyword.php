<?php

require_once 'Model.php';

class Keyword extends Model
{
	protected $table = 'keyword';
    protected $fields = ['word' => PDO::PARAM_STR];
}

