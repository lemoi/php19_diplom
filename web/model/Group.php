<?php

require_once 'Model.php';

class Group extends Model
{
	protected $table = 'group';
    protected $fields = ['name' => PDO::PARAM_STR];

	/**
    * Удаление категории
	* @param $id int
	* @return mixed
	*/
	function delete($id)
    {
        /*
        $pdo = $this->db;
        try {
            $pdo->beginTransaction();

            // В одной транзакции удалим все вопросы по удаляемой категории
            $sth = $this->db->prepare("DELETE FROM `question` WHERE `group_id`=:id");
            $sth->bindValue(':id', $id, PDO::PARAM_INT);
            $result = $sth->execute();

            // Если все ОК, удалим и саму категорию
            if ($result) {
                $result = parent::delete($id);
            }

            if ($result) {
                $pdo->commit();
            } else {
                throw new Exception('Неизвестная ошибка работы с базой данных при удалении категории');
            }
        } catch (Exception $e) {
            $pdo->rollBack();
            echo "Ошибка: " . $e->getMessage();
        }
        return $result;
        */
        return parent::delete($id); // на уровне таблицы 'question' добавлен внеший ключ на id группы с автоматическим удалением. Особый алгоритм не требуется
    }

	/**
	* Получение всех категорий
	* @return array
	*/
	public function findAll()
	{
        $keys = array_keys($this->fields);
        //$sql = 'SELECT `id`, `' . implode('`, `', $keys) . '` FROM `' . $this->table . '`';
        $sql = <<<EOT
        SELECT t1.`id`, t1.`name`,
        COUNT(t2.`id`) AS questions_total,
        SUM(IF(IFNULL(t2.`is_hidden`, 0) OR IFNULL(TRIM(t2.`answer`) = '', 1), 0, 1)) AS questions_public,
        SUM(IFNULL(TRIM(t2.`answer`) = '', 0)) AS questions_unanswered
        FROM `group` AS t1 
        LEFT JOIN `question` AS t2 
        ON t1.`id` = t2.`group_id` 
        GROUP BY t1.`id`, t1.`name`
EOT;
        $sth = $this->db->prepare($sql);
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		return false;
	}
}

