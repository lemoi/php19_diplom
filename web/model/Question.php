<?php

require_once 'Model.php';

class Question extends Model
{
    protected $table = 'question';
    protected $fields = [
	    'description' => PDO::PARAM_STR,
        'answer' => PDO::PARAM_STR,
        'date_added' => PDO::PARAM_STR,
        'is_hidden' => PDO::PARAM_INT,
        'author_name' => PDO::PARAM_STR,
        'author_email' => PDO::PARAM_STR,
        'group_id' => PDO::PARAM_INT,
        'moderation' => PDO::PARAM_INT,
    ];
    protected $autoFields = ['date_added', 'is_hidden', 'answer', 'moderation'];

    function add($params)
    {
        $id = parent::add($params);
        if ($id !== false) {
            $this->requiredModeration($id);
        }
        return $id;
    }

    /**
	* Получение всех вопросов
	* @return array
	*/
	public function findAll($conf = null)
	{
        $keys = array_keys($this->fields);
        $accord = ['question' => 't1', 'group' => 't2'];
        $fields = [];
        foreach ($keys as $key) {
            $fields[] = 't1.`' . $key . '`';
        }
        $fields[] = 't1.`id` as `id`';
        $fields[] = 't2.`name` as `group_name`';
        /*$query = 'SELECT t1.`id`, `description`, `answer`, `date_added`, `is_hidden`, `author_name`, `author_email`, `group_id`, t2.`name` as `group_name`'
          .'FROM `question` as t1 LEFT JOIN `group` as t2 ON t1.`group_id` = t2.`id`';*/
        //$query = 'SELECT ' . implode(', ', $fields) . 'FROM `' . $this->table . '` as t1 LEFT JOIN `group` as t2 ON t1.`group_id` = t2.`id` ORDER BY t1.`date_added`';
        $query = 'SELECT ' . implode(', ', $fields) . ' FROM `' . $this->table . '` as t1 LEFT JOIN `group` as t2 ON t1.`group_id` = t2.`id`';
        $fields = [];
        $order = [];
        $values = [];
        if (isset($conf) && count($conf)) {
            foreach ($conf as $section => $conf2) {
                foreach ($conf2 as $table => $conf3) {
                    foreach ($conf3 as $key => $value) {
                        if ($section == 'where') {
                            $tmp = isset($value[2]) ? $value[2] : '%%';
                            $fields[] = str_replace($tmp, '%%', "{$accord[$table]}.`{$key}` ") . $value[1] . " :{$key}";
                            $values[$key] = $value[1] == 'LIKE' ? "%{$value[0]}%" : $value[0];
                        } elseif ($section == 'order') {
                            $order[] = "{$accord[$table]}.`{$key}` $value";
                        }
                    }
                }
            }
        }
        if (count($fields)) {
            $query .= ' WHERE ' . implode(' AND ', $fields);
        }
        if (count($order)) {
            $query .= ' ORDER BY ' . implode(', ', $order);
        }
        $sth = $this->db->prepare($query);
        if (count($values)) {
            foreach ($values as $key => $value) {
                $sth->bindValue(':' . $key, $value);
            }
        }
		if ($sth->execute()) {
			return $sth->fetchAll();
		}
		var_dump('error');
		return false;
	}

	// Получение списка групп
	public function getTotalGroup()
    {
        $result = [];

        $sql = 'SELECT `id`, `name` FROM `group`';
        foreach ($this->db->query($sql) as $row) {
            $result['id' . $row['id']] = $row['name'];
        }

        return $result;
    }

    public function find($id)
    {
        $conf['where']['question']['id'] = [$id, '='];
        $result = $this->findAll($conf);
        return empty($result) ? false : $result[0];
    }

    public function requiredModeration($id)
    {
        $keywords = $this->db->query('SELECT `word` FROM `keyword`')->fetchAll(PDO::FETCH_COLUMN, 0);
        if (!count($keywords)) {
            return;
        }

        $state = true;
        /*
        $question = $this->find($id);
        foreach ($keywords as $keyword) {
            if (strpos($question['description'], $keyword) !== false) {
                $state = false;
                break;
            }
        }
        */

        $sql = "SELECT `id`, `description` FROM `question` WHERE MATCH (`description`) AGAINST (:keywords) AND `id` = :id";
        $sth = $this->db->prepare($sql);
        $sth->bindValue(':keywords', implode(' ', $keywords));
        $sth->bindValue(':id', $id);
        if ($sth->execute()) {
            $state = empty($sth->fetchAll());
        }

        if (!$state) {
            $this->update($id, ['moderation' => 1]); // отправим на модерацию
        }
    }

    public function getListModeration()
    {
        $sql = <<<EOT
            SELECT 
              t1.`id` as id,
              MAX(t1.`description`) as description,
              MAX(t1.`author_name`) as author_name,
              MAX(t1.`author_email`) as author_email,
              MAX(t1.`date_added`) as date_added,
              MAX(t3.`name`) as group_name,
              GROUP_CONCAT(t2.`word`) as words 
            FROM `question` as t1 
              LEFT JOIN `keyword` as t2 ON t1.description RLIKE CONCAT('[[:<:]]', t2.word, '[[:>:]]')
              LEFT JOIN `group` as t3 ON t1.`group_id` = t3.`id`
            WHERE 
               t1.`moderation`
            GROUP BY 
              id
EOT;
        return $this->db->query($sql)->fetchAll();
    }

    public function getView($id)
    {
        $result = $this->find($id);
        if (empty($result)) {
            return "вопрос имеющий id = {$id} в БД отсутствует";
        }

        return "(${id}) из темы \"{$result['group_name']}\" ({$result['group_id']})";
    }
}

