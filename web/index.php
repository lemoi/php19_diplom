<?php

require_once 'lib/functions.php'; // Общие функции

require_once '../vendor/autoload.php'; // Подключение twig

include 'lib/database/DataBase.php'; // Подключение к базе данных

include 'lib/router/Router.php'; // Запуск роутинга
