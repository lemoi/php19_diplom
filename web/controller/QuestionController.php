<?php

require_once 'Controller.php';

class QuestionController extends Controller
{
    protected $name = 'question';
    protected $declination = ['вопрос', 'вопрос'];
    protected $fields = [
        'author_name',
        'author_email',
        'description',
        'group',
    ];

    /**
     * Форма добавление вопроса
     * @param $params array
     * @return mixed
     */
    function getAdd()
    {
        $groups = $this->model->getTotalGroup();
        $params = [
            'groups' => $groups,
            'errors' => [],
            'author_name' => $_SESSION['author_name'],
            'author_email' => $_SESSION['author_email'],
        ];
        echo $this->render($this->name . '/add.twig', $params);
    }

    /**
     * Добавление вопроса
     * @param $params array
     * @return mixed
     */
    function postAdd($params, $post)
    {
        $groups = $this->model->getTotalGroup();

        $params = $post;
        $params['groups'] = $groups;
        $params['errors'] = [];
        if (!$this->paramsFill($post) || !array_key_exists($post['group'], $groups)) {
            $params['errors'][] = 'Для добавления вопроса необходимо заполнить все поля';
            echo $this->render2($this->name . '/add.twig', $params);
            die;
        }

        $fields = [];
        $keys = $this->model->getFields();
        foreach ($keys as $key) {
            if (isset($post[$key])) {
                $fields[$key] = $post[$key];
            }
        }
        $fields['group_id'] = str_replace('id', '', $post['group']);

        $id = $this->model->add($fields);
        if ($id) {
            $_SESSION['author_name'] = $post['author_name'];
            $_SESSION['author_email'] = $post['author_email'];
            //addLog("создал {$this->declination[1]} {$this->model->getView($id)}"); // вопросы добавляет не администратор, по заданию логировать это не нужно

            //header('Location: /');
            //header('Location: ' . $_SERVER['PHP_SELF']);
            $this->getList();
            die;
        } else {
            $params['errors'][] = 'Вопрос не удалось добавить';
        };

        echo $this->render($this->name . '/add.twig', $params);
    }

    /**
     * Удаление вопроса
     * @param $id
     */
    public function getDelete($params)
    {
        if (isset($params['id']) && is_numeric($params['id'])) {
            $id = $params['id'];
            $view = $this->model->getView($id);
            $isDelete = $this->model->delete($id);
            if ($isDelete) {
                addLog("удалил {$this->declination[1]} {$view}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getListAdmin();
                die;
            }
        }
    }

    /**
     * Форма редактирование вопроса
     * @param $id
     */
    public function getUpdate($params)
    {
        if (isset($params['id']) && is_numeric($params['id'])) {
            $id = $params['id'];

            $question = $this->model->find($id);
            $groups = $this->model->getTotalGroup();
            $params = [
                'groups' => $groups,
                'group' => 'id' . $question['group_id'],
                'question' => $question,
            ];
            echo $this->render($this->name . '/update.twig', $params);
        }
    }

    /**
     * Изменение вопроса
     * @param $id
     */
    public function postUpdate($params, $post)
    {
        if (isset($params['id']) && is_numeric($params['id']) && $this->paramsFill($post)) {
            $id = $params['id'];
            $view = $this->model->getView($id);

            $updateParam = [];
            $keys = $this->model->getFields();
            foreach ($keys as $key) {
                if (isset($post[$key])) {
                    $updateParam[$key] = $post[$key];
                }
            }
            $updateParam['is_hidden'] = !empty($post['is_hidden']);
            $updateParam['moderation'] = !empty($post['moderation']);
            $updateParam['group_id'] = str_replace('id', '', $post['group']);

            $oldState = $this->model->find($id);
            $isUpdate = $this->model->update($id, $updateParam);
            if ($isUpdate) {
                $newState = $this->model->find($id);
                $message = "{$this->declination[1]} {$view}";
                if (!empty($newState['answer']) && $newState['answer'] != $oldState['answer']) {
                    $message = 'ответил на ' . $message;
                } else if (!$newState['is_hidden'] && $newState['is_hidden'] != $oldState['is_hidden']) {
                    $message = 'скрыл ' . $message;
                } else if ($newState['is_hidden'] && $newState['is_hidden'] != $oldState['is_hidden']) {
                    $message = 'опубликовал ' . $message;
                } else {
                    $message = 'обновил ' . $message;
                }
                addLog($message);

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getListAdmin();
                die;
            }
        }
        $this->getUpdate($params);
    }

    /**
     * Получение всех вопросов
     * @return array
     */
    public function getList($params = [])
    {
        $questions = [];
        $groups = [];
        $conf['order']['group']['name'] = 'ASC';
        $conf['where']['question']['answer'] = ['', '<>'];
        $conf['where']['question']['is_hidden'] = [0, '='];
        foreach ($this->model->findAll($conf) as $value) {
            $group = 'group_' . $value['group_id'];
            $questions[$group][] = $value;

            if (!array_key_exists($group, $groups)) {
                $groups[$group] = $value['group_name'];
            }
        }
        $params['questions'] = $questions;
        $params['groups'] = $groups;
        echo $this->render('index.twig', $params);
    }

    /**
     * Получение всех вопросов для Админа
     * @return array
     */
    public function getListAdmin($params = [])
    {
        if (!empty($params)) {
            $_SESSION['params'] = $params;
        }
        if (is_array($_SESSION['params'])) {
            $params = array_merge($_SESSION['params'], $params);
        }
        $params = array_merge([
            'group' => '',
            'author' => '',
            'not_answer' => 0,
            'moderation' => 0,
        ], $params);

        $params['sort_kind'] = [
            'date_added' => 'Дате (возр.)',
            'date_added_desc' => 'Дате (убыв.)',
        ];
        if (empty($params['sort_by'])) {
            $params['sort_by'] = key($params['sort_kind']);
        }
        if (isset($params['group_id']) && empty($params['group'])) {
            $groups = $this->model->getTotalGroup();
            $params['group'] = $groups['id' . $params['group_id']];
        }

        $conf = [];
        if (!empty($params['group'])) {
            $conf['where']['group']['name'] = [$params['group'], 'LIKE'];
        }
        if (!empty($params['author'])) {
            $conf['where']['question']['author_name'] = [$params['author'], 'LIKE'];
        }
        if (!empty($params['not_answer'])) {
            $conf['where']['question']['answer'] = ['', '='];
        }
        if (!empty($params['moderation'])) {
            $conf['where']['question']['moderation'] = [1, '='];
        }
        if (!empty($params['sort_by'])) {
            $desc = substr($params['sort_by'], -5) == '_desc';
            $sort = $desc ? substr($params['sort_by'], 0, -5) : $params['sort_by'];
            $conf['order']['question'][$sort] = $desc ? 'DESC' : 'ASC';
        }

        $params['questions'] = $this->model->findAll($conf);
        echo $this->render($this->name . '/listAdmin.twig', $params);
    }

    /**
     * Форма списка заблокированных вопросов
     */
    public function getListModeration($params = [])
    {
        $params['questions'] = $this->model->getListModeration();
        echo $this->render($this->name . '\listModeration.twig', $params);
    }

    /**
     * Снятие отметки нахождения вопроса на модерации
     */
    public function getСhecked($params)
    {
        if (isset($params['id']) && is_numeric($params['id'])) {
            $id = $params['id'];

            $isUpdate = $this->model->update($id, ['moderation' => 0]);
            if ($isUpdate) {
                addLog("проверил {$this->declination[1]} {$this->model->getView($id)}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getListModeration();
                die;
            }
        }
    }

    public function postSelect($params, $post)
    {
        $this->getListAdmin(array_merge($params, $post));
    }

}

