<?php

class Controller
{
    protected $model = null;
    protected $name = 'controller';
    protected $declination = ['контроллер', 'контроллер'];
    protected $fields = [];

    function __construct()
    {
        $className = ucfirst($this->name);
        include 'model/' . $className . '.php';
        $this->model = new $className();
    }

    /**
     * Отображаем шаблон
     * @param $template
     * @param $params
     */
    protected function render($template, $params = [])
    {
        $loader = new Twig_Loader_Filesystem('./template');
        $twig = new Twig_Environment($loader, [
            'cache' => './tmp/cache',
            'auto_reload' => true,
        ]);

        $params['navigation'] = getNavigation();

        return $twig->render($template, $params);
    }

    protected function paramsFill($params)
    {
        foreach ($this->fields as $key) {
            if (!array_key_exists($key, $params) || empty($params[$key])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Форма добавления объекта
     * @param $params array
     * @return mixed
     */
    public function getAdd()
    {
        echo $this->render($this->name . '/add.twig');
    }

    /**
     * Обработка формы добавления объекта
     * @param $params array
     * @return mixed
     */
    function postAdd($params, $post)
    {
        if ($this->paramsFill($post)) {
            $fields = [];
            $keys = $this->model->getFields();
            foreach ($keys as $key) {
                if (isset($post[$key])) {
                    $fields[$key] = $post[$key];
                }
            }

            $id = $this->model->add($fields);
            if ($id !== false) {
                addLog("создал {$this->declination[1]} {$this->model->getView($id)}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getList();
                die;
            };
        }
        $this->getAdd();
    }

    /**
     * Форма редактирование объекта
     * @param $id
     */
    public function getUpdate($params)
    {
        if (isset($params['id']) && is_numeric($params['id'])) {
            $id = $params['id'];
            $params = [
                $this->name => $this->model->find($id)
            ];
            echo $this->render($this->name . '/update.twig', $params);
        }
    }

    /**
     * Обработка формы изменения объекта
     * @param $id
     */
    public function postUpdate($params, $post)
    {
        if (isset($params['id']) && is_numeric($params['id']) && $this->paramsFill($post)) {
            $id = $params['id'];

            $updateParam = [];
            $keys = $this->model->getFields();
            foreach ($keys as $key) {
                if (isset($post[$key])) {
                    $updateParam[$key] = $post[$key];
                }
            }

            $isUpdate = $this->model->update($id, $updateParam);
            if ($isUpdate) {
                addLog("обновил {$this->declination[1]} {$this->model->getView($id)}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getList();
                die;
            }
        }
        $this->getUpdate($params);
    }

    /**
     * Удаление объекта
     * @param $id
     */
    public function getDelete($params)
    {
        if (isset($params['id']) && is_numeric($params['id'])) {
            $id = $params['id'];
            $view = $this->model->getView($id);

            $isDelete = $this->model->delete($id);
            if ($isDelete) {
                addLog("удалил {$this->declination[1]} {$view}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getList();
                die;
            }
        }
    }

    /**
     * Форма получение всех объектов
     * @return array
     */
    public function getList()
    {
        $params = [
            $this->name . 's' => $this->model->findAll()
        ];
        echo $this->render($this->name . '/list.twig', $params);
    }
}