<?php

require_once 'Controller.php';

class GroupController extends Controller
{
    protected $name = 'group';
    protected $declination = ['тема', 'тему'];
    protected $fields = [
        'name'
    ];
}

