<?php

require_once 'Controller.php';

class KeywordController extends Controller
{
    protected $name = 'keyword';
    protected $declination = ['ключевое слово', 'ключевое слово'];
    protected $fields = [
        'word'
    ];
}

