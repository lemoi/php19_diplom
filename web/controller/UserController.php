<?php

require_once 'Controller.php';

class UserController extends Controller
{
    protected $name = 'user';
    protected $declination = ['администратор', 'администратора'];
    protected $fields = [
        'login',
        'password',
    ];

    /**
     * Форма авторизации
     * @return mixed
     */
    function getAuth()
    {
        $user = ['login' => ''];
        $params = [
            'user' => $user,
            'errors' => []
        ];
        echo $this->render($this->name . '/auth.twig', $params);
    }

    /**
     * Авторизация пользователя
     * @param $params array
     * @return mixed
     */
    function postAuth($params, $post)
    {
        $errors = [];
        if ($this->paramsFill($post)) {

            $searchField = ['login' => $post['login'], 'password' => $post['password']];
            $user = $this->model->find($searchField);

            if ($user) {
                $_SESSION['user'] = $user;
                //header('Location: /');
                header('Location: ' . $_SERVER['PHP_SELF']);
            } else {
                $errors[] = 'Пользователь не найден. Вероятно вы ошиблись при указании логина или пароля';
            };
        } else {
            $errors[] = 'Необходимо заполнить ваши логин и пароль перед выполнением авторизации';
        }

        $params = [
            'user' => ['login' => $post['login']],
            'errors' => $errors
        ];
        echo $this->render($this->name . '/auth.twig', $params);
    }

    function leave()
    {
        session_destroy();
        header('Location: ' . $_SERVER['PHP_SELF']);
    }

    /**
     * Обработка формы добавления объекта
     * @param $params array
     * @return mixed
     */
    function postAdd($params, $post)
    {
        if ($this->paramsFill($post)) {
            $searchField = ['login' => $post['login']];
            $user = $this->model->find($searchField);
            if ($user) {
                // пользователь с таким логином существует
                $this->getAdd();
                die;
            }

            $fields = [];
            $keys = $this->model->getFields();
            foreach ($keys as $key) {
                if (isset($post[$key])) {
                    $fields[$key] = $post[$key];
                }
            }

            $id = $this->model->add($fields);
            if ($id) {
                addLog("создал {$this->declination[1]} {$this->model->getView($id)}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getList();
                die;
            };
        }
        $this->getAdd();
    }

    /**
     * Обработка формы изменения объекта
     * @param $id
     */
    public function postUpdate($params, $post)
    {
        if (isset($params['id']) && is_numeric($params['id']) && $this->paramsFill($post)) {
            $id = $params['id'];

            $searchField = ['login' => $post['login']];
            $user = $this->model->find($searchField);
            if ($user) {
                // пользователь с таким логином существует
                $this->getUpdate($params);
                die;
            }

            $updateParam = [];
            $keys = $this->model->getFields();
            foreach ($keys as $key) {
                if (isset($post[$key])) {
                    $updateParam[$key] = $post[$key];
                }
            }

            $isUpdate = $this->model->update($id, $updateParam);
            if ($isUpdate) {
                addLog("обновил {$this->declination[1]} {$this->model->getView($id)}");

                //header('Location: /');
                //header('Location: ' . $_SERVER['PHP_SELF']);
                $this->getList();
                die;
            }
        }
        $this->getUpdate($params);
    }
}
