<?php

class Router
{
    private $dirConroller = '';
    private $urls = [];

    function __construct($dirConroller)
    {
        $this->dirConroller = $dirConroller;
    }

    /**
     * Добавление роутеров
     * @param $url урл
     * @param $controllerAndAction пример: UserController@getUpdate
     */
    public function get($url, $controllerAndAction, $params = [])
    {
        $this->add('GET', $url, $controllerAndAction, $params);
    }

    /**
     * Добавление роутеров
     * @param $url урл
     * @param $controllerAndAction пример: UserController@postUpdate
     */
    public function post($url, $controllerAndAction, $params = [])
    {
        $this->add('POST', $url, $controllerAndAction, $params);
    }

    /**
     * Добавление роутеров
     * @param $url урл
     * @param $controllerAndAction пример: UserController@list
     */
    public function add($method, $url, $controllerAndAction, $params)
    {
        list($controller, $action) = explode('@', $controllerAndAction);

        $this->urls[$method][$url] = [
            'controller' => $controller,
            'action' => $action,
            'params' => $params
        ];
    }

    /**
     * Подключение контроллеров
     * @param $url текущий урл
     */
    public function run($currentUrl)
    {
        $routeExist = false;
        if (isset($this->urls[$_SERVER['REQUEST_METHOD']])) {
            foreach ($this->urls[$_SERVER['REQUEST_METHOD']] as $url => $urlData) {
                if (preg_match('(^'.$url.'$)', $currentUrl, $matchList)) {
                    $routeExist = true;
                    $params = [];
                    foreach ($urlData['params'] as $param => $i) {
                        $params[$param] = $matchList[$i];
                    }
                    include $this->dirConroller.$urlData['controller'].'.php';
                    $controller = new $urlData['controller']();
                    $action = $urlData['action'];
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        //$controller->$urlData['action']($params, $_POST);
                        $controller->$action($params, $_POST);
                    } else {
                        //$controller->$urlData['action']($params);
                        $controller->$action($params);
                    }
                }
            }
        }
        if (!$routeExist) {
            die ('404 Not Found');
        }
    }
}

$router = new Router('controller/');

$router->get('/', 'QuestionController@getList');

$router->get('/question/add/', 'QuestionController@getAdd');
$router->post('/question/add/', 'QuestionController@postAdd');

if (isAuth()) {
    $router->get('/user/leave/', 'UserController@leave');

    $router->get('/user/list/', 'UserController@getList');
    $router->get('/user/add/', 'UserController@getAdd');
    $router->post('/user/add/', 'UserController@postAdd');

    $router->get('/user/update/id/(\d+)/', 'UserController@getUpdate', ['id' => 1]);
    $router->post('/user/update/id/(\d+)/', 'UserController@postUpdate', ['id' => 1]);
    $router->get('/user/delete/id/(\d+)/', 'UserController@getDelete', ['id' => 1]);

    $router->get('/group/list/', 'GroupController@getList');
    $router->get('/group/add/', 'GroupController@getAdd');
    $router->post('/group/add/', 'GroupController@postAdd');
    $router->get('/group/update/id/(\d+)/', 'GroupController@getUpdate', ['id' => 1]);
    $router->post('/group/update/id/(\d+)/', 'GroupController@postUpdate', ['id' => 1]);
    $router->get('/group/delete/id/(\d+)/', 'GroupController@getDelete', ['id' => 1]);

    $router->get('/question/update/id/(\d+)/', 'QuestionController@getUpdate', ['id' => 1]);
    $router->post('/question/update/id/(\d+)/', 'QuestionController@postUpdate', ['id' => 1]);
    $router->get('/question/delete/id/(\d+)/', 'QuestionController@getDelete', ['id' => 1]);

    $router->get('/question/list/', 'QuestionController@getListAdmin');
    $router->post('/question/select/', 'QuestionController@postSelect');
    $router->get('/question/list/group_id/(\d+)/', 'QuestionController@getListAdmin', ['group_id' => 1]);

    $router->get('/question/listModeration/', 'QuestionController@getListModeration');
    $router->get('/question/checked/id/(\d+)/', 'QuestionController@getСhecked', ['id' => 1]);

    $router->get('/keyword/list/', 'KeywordController@getList');
    $router->get('/keyword/add/', 'KeywordController@getAdd');
    $router->post('/keyword/add/', 'KeywordController@postAdd');
    $router->get('/keyword/update/id/(\d+)/', 'KeywordController@getUpdate', ['id' => 1]);
    $router->post('/keyword/update/id/(\d+)/', 'KeywordController@postUpdate', ['id' => 1]);
    $router->get('/keyword/delete/id/(\d+)/', 'KeywordController@getDelete', ['id' => 1]);

} else {
    $router->get('/user/auth/', 'UserController@getAuth');
    $router->post('/user/auth/', 'UserController@postAuth');
}

/*
Удаляем "/?", потому что не сделали настройки на серверах
 */
//$currentUrl = str_replace('/?', '', $_SERVER['REQUEST_URI']);
$currentUrl = empty($_SERVER['QUERY_STRING']) ? '/' : $_SERVER['QUERY_STRING'];

/*
Если добавить конфиг в
Apache
	Options +FollowSymLinks
	RewriteEngine On
	RewriteRule ^(.*)$ index.php [NC,L]

Nginx:
	location / {
		try_files $uri $uri/ /index.php?$query_string;
	}

то:
$currentUrl = $_SERVER['REQUEST_URI'];
*/
$router->run($currentUrl);
