<?php

class DataBase
{

    protected static $db;
    protected static $dbName;

    /**
     * Подключение к базе данных mysql
     * @param $host адрес
     * @param $dbname название базы
     * @param $user пользователь
     * @param $pass пароль
     */
    public static function connect()
    {
        $config = include_once 'config.php';
        $param = $config['mysql'];
        self::$dbName = $param['dbname'];

        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            //$db = new PDO('mysql:host=' . $param['host'] . ';dbname=' . $param['dbname'] . ';charset=utf8', $param['user'], $param['pass'], $opt);
            $db = new PDO('mysql:host=' . $param['host'] . ';charset=utf8', $param['user'], $param['pass'], $opt);
        } catch (PDOException $e) {
            die('Database error: ' . $e->getMessage() . '<br/>');
        }
        return $db;
    }

    public static function getCoonection()
    {
        if (self::$db === null) {
            self::$db = self::connect();
            self::init();
        }
        return self::$db;
    }

    protected static function init()
    {
        $dbName = self::$dbName;
        $result = self::$db->query("SHOW DATABASES LIKE '${dbName}'")->fetchAll(PDO::FETCH_COLUMN, 0);
        if (empty($result)) {
            self::$db->exec("CREATE DATABASE ${dbName}");
        }

        self::$db->exec("USE ${dbName}");
        $result = self::$db->query("SHOW TABLES LIKE 'question'")->fetchAll(PDO::FETCH_COLUMN, 0);
        if (empty($result)) {
            //self::import(__DIR__ . '/../../faq.sql');
            self::import(__DIR__ . '/../../faq_data.sql');
            addLog('инициализация базы данных');
        }
    }

    // https://github.com/tazotodua/useful-php-scripts
    public static function import($sql_file_OR_content)
    {
        set_time_limit(3000);
        $SQL_CONTENT = (strlen($sql_file_OR_content) > 300 ? $sql_file_OR_content : file_get_contents($sql_file_OR_content));
        $allLines = explode("\n", $SQL_CONTENT);
        $db = self::getCoonection();
        $db->beginTransaction();
        $db->exec('SET foreign_key_checks = 0');
        preg_match_all("/\nCREATE TABLE(.*?)\`(.*?)\`/si", "\n" . $SQL_CONTENT, $target_tables);
        foreach ($target_tables[2] as $table) {
            $db->exec("DROP TABLE IF EXISTS `${table}`");
        }
        $db->exec('SET foreign_key_checks = 1');
        $db->exec("SET NAMES 'utf8'");
        $templine = '';    // Temporary variable, used to store current query
        foreach ($allLines as $line) { // Loop through each line
            if (substr($line, 0, 2) != '--' && $line != '') {
                $templine .= $line; // (if it is not a comment..) Add this line to the current segment
                if (substr(trim($line), -1, 1) == ';') { // If it has a semicolon at the end, it's the end of the query
                    if ($db->exec($templine) === false) {
                        print('Error performing query \'<strong>' . $templine . '\': ' . implode(',', $db->errorInfo()) . '</strong><br>');
                    }
                    $templine = ''; // set variable to empty, to start picking up the lines after ";"
                }
            }
        }
        $db->commit();
        return 'Importing finished. Now, Delete the import file.';
    }

}
