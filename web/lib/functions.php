<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
ini_set('display_errors', 1);

session_start();

function isAuth() {
    return isset($_SESSION['user']) ? true : false;
}

function getNavigation() {
    return [
        ['href' => '?', 'desc' => 'FAQ (главная)'],
        ['href' => '?/question/add/', 'desc' => 'Добавить вопрос'],
        ['href' => '?/user/list/', 'desc' => 'Администраторы', 'hide' => !isAuth()],
        ['href' => '?/group/list/', 'desc' => 'Категории', 'hide' => !isAuth()],
        ['href' => '?/question/list/', 'desc' => 'Вопросы', 'hide' => !isAuth()],
        ['href' => '?/keyword/list/', 'desc' => 'Ключевые слова', 'hide' => !isAuth()],
        ['href' => '?/question/listModeration/', 'desc' => 'Заблокированные вопросы', 'hide' => !isAuth()],
        ['href' => '?/user/auth/', 'desc' => 'Войти', 'hide' => isAuth()],
        ['href' => '?/user/leave/', 'desc' => 'Выйти', 'hide' => !isAuth()],
    ];
}

function addLog($message) {
    $log_file_name = isHeroku() ? 'php://stderr' : $_SERVER['DOCUMENT_ROOT'] . "/log_faq.txt";
    $user = $_SESSION['user']['login'];
    $now = date("Y-m-d H:i:s");
    file_put_contents($log_file_name, "${now} ${user} ${message}" . PHP_EOL, FILE_APPEND);
}

function isHeroku() {
    return $_SERVER['SERVER_NAME'] == 'lemoi-php19-diplom.herokuapp.com';
}